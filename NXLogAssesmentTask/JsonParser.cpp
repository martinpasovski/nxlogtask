#include "JsonParser.h"
#include <cstdint>
#include <fstream>
#include <sstream>
#include <iostream>
#include <sys/stat.h>
#include <list>
#include <algorithm>

#include <rapidjson/1.2.0/include/writer.h>
#include <rapidjson/1.2.0/include/prettywriter.h>
#include <rapidjson/1.2.0/include/stringbuffer.h>

RecordInfo JsonParser::processJsonRecord(const std::string& fileName, const std::stringstream& jsonRecord)
{
    RecordInfo recordInfo;
    rapidjson::Document doc;
    doc.Parse(jsonRecord.str().c_str());
    if (doc.HasParseError())
    {
        std::cout << "JsonParser::processJsonRecord() Invalid record (line) in file: " << fileName << std::endl;
        return recordInfo;
    }

    for (rapidjson::Value::ConstMemberIterator dbObjectIt = doc.MemberBegin(); dbObjectIt != doc.MemberEnd(); ++dbObjectIt)
    {
        // Save recordEntry to record
        std::string recordEntryKey = dbObjectIt->name.GetString();
        if (!dbObjectIt->value.IsNull() && !dbObjectIt->value.IsArray())
        {
            if (dbObjectIt->value.IsString())
                recordInfo.getRecord().append(new StrRecordEntry(recordEntryKey, std::string(dbObjectIt->value.GetString())));
            else if (dbObjectIt->value.IsBool())
                recordInfo.getRecord().append(new BoolRecordEntry(recordEntryKey, dbObjectIt->value.GetBool()));
            else if (dbObjectIt->value.IsInt())
                recordInfo.getRecord().append(new IntRecordEntry(recordEntryKey, dbObjectIt->value.GetInt()));
            else
            {
                std::cout << "JsonParser::processJsonRecord() Unexpected record entry type for record entry key: " <<
                    dbObjectIt->name.GetString() << " in the data base json file:" << fileName << std::endl;
                return recordInfo;
            }
        }
        // Report error
        else
        {
            std::cout << "JsonParser::processJsonRecord() Record entry empty or unexpected complex type for record entry key: " << 
                dbObjectIt->name.GetString() << " in the data base json file:" << fileName << std::endl;
            return recordInfo;
        }
    }

    return recordInfo;
}


