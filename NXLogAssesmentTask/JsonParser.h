#pragma once
#include "Record.h"
#include <rapidjson/1.2.0/include/rapidjson.h>
#include <rapidjson/1.2.0/include/document.h>

class JsonParser : public std::enable_shared_from_this<JsonParser>
{
public:
	RecordInfo processJsonRecord(const std::string& fileName, const std::stringstream& jsonRecord);
};

