#pragma once
#include "Record.h"

class RecordService
{
public:
	void encodeRecord(RecordInfo & recordInfo);
	void decodeRecord(RecordInfo& recordInfo);

	void saveBinaryRecord(RecordInfo& recordInfo, const char* filename, bool createEncodedRecords);
	void loadBinaryRecord(RecordInfo& recordInfo, const char* filename, bool restoreEncodedRecords);
};

