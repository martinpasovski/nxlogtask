#pragma once
#include <any>
#include <memory>
#include <list>
#include <vector>
#include <map>
#include <unordered_map>

#include <cstddef> // NULL
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

#include <boost/archive/tmpdir.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

class BaseRecordEntry
{
	friend class boost::serialization::access;
	friend std::ostream& operator<<(std::ostream& os, const BaseRecordEntry& bre);
	virtual std::string description() const = 0;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int /* file_version */) {
		ar& m_Key;
	}

protected:
	BaseRecordEntry(std::string key) : m_Key(key) {}

public:
	BaseRecordEntry() {}
	virtual ~BaseRecordEntry() {}

	virtual std::string getKey() { return m_Key; }

protected:
	std::string m_Key;
};

class IntRecordEntry : public BaseRecordEntry
{
	friend class boost::serialization::access;
	virtual std::string description() const override
	{
		return std::to_string(m_Value);
	}
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		// save/load base class information
		ar& boost::serialization::base_object<BaseRecordEntry>(*this);
		ar& m_Value;
	}

public:
	IntRecordEntry() {}
	IntRecordEntry(std::string key, int val) :
		BaseRecordEntry(key),
		m_Value(val)
	{}
	~IntRecordEntry() {}

	int getValue() { return m_Value; }

private:
	int m_Value;
};

class BoolRecordEntry : public BaseRecordEntry
{
	friend class boost::serialization::access;
	virtual std::string description() const override
	{
		return m_Value ? "TRUE" : "FALSE";
	}
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		// save/load base class information
		ar& boost::serialization::base_object<BaseRecordEntry>(*this);
		ar& m_Value;
	}

public:
	BoolRecordEntry() {}
	BoolRecordEntry(std::string key, bool val) :
		BaseRecordEntry(key),
		m_Value(val)
	{}
	~BoolRecordEntry() {}

	bool getValue() { return m_Value; }

private:
	bool m_Value;
};

class StrRecordEntry : public BaseRecordEntry
{
	friend class boost::serialization::access;
	virtual std::string description() const override
	{
		return m_Value;
	}
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		// save/load base class information
		ar& boost::serialization::base_object<BaseRecordEntry>(*this);
		ar& m_Value;
	}

public:
	StrRecordEntry() {}
	StrRecordEntry(std::string key, std::string val) :
		BaseRecordEntry(key),
		m_Value(val)
	{}
	~StrRecordEntry() {}

	std::string getValue() { return m_Value; }

private:
	std::string m_Value;
};

class Record
{
	friend class boost::serialization::access;
	friend std::ostream& operator<<(std::ostream& os, const Record& r);
	std::list<BaseRecordEntry*> m_RecordEntries;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		ar.register_type(static_cast<IntRecordEntry*>(NULL));
		ar.register_type(static_cast<BoolRecordEntry*>(NULL));
		ar.register_type(static_cast<StrRecordEntry*>(NULL));

		ar& m_RecordEntries;
	}
public:
	Record() {}
	std::list<BaseRecordEntry*>& getRecordEntries() { return m_RecordEntries; }
	void append(BaseRecordEntry* pBre)
	{
		m_RecordEntries.insert(m_RecordEntries.end(), pBre);
	}
	void clear()
	{
		for (auto recordEntry : m_RecordEntries)
			delete recordEntry;
	}
};

/******************************/
/*** Encoded Record classes ***/
/******************************/
class BaseEncodedRecordEntry
{
	friend class boost::serialization::access;
	friend std::ostream& operator<<(std::ostream& os, const BaseEncodedRecordEntry& bere);
	virtual std::string description() const = 0;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int /* file_version */) {
		ar& m_Key;
	}

protected:
	BaseEncodedRecordEntry(int key) : m_Key(key) {}

public:
	BaseEncodedRecordEntry() {}
	virtual ~BaseEncodedRecordEntry() {}

	virtual int getKey() { return m_Key; }

protected:
	int m_Key;
};

class IntEncodedRecordEntry : public BaseEncodedRecordEntry
{
	friend class boost::serialization::access;
	virtual std::string description() const override
	{
		return std::to_string(m_Value);
	}
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		// save/load base class information
		ar& boost::serialization::base_object<BaseEncodedRecordEntry>(*this);
		ar& m_Value;
	}

public:
	IntEncodedRecordEntry() {}
	IntEncodedRecordEntry(int key, int val) :
		BaseEncodedRecordEntry(key),
		m_Value(val)
	{}
	~IntEncodedRecordEntry() {}

	int getValue() { return m_Value; }

private:
	int m_Value;
};

class BoolEncodedRecordEntry : public BaseEncodedRecordEntry
{
	friend class boost::serialization::access;
	virtual std::string description() const override
	{
		return m_Value ? "TRUE" : "FALSE";
	}
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		// save/load base class information
		ar& boost::serialization::base_object<BaseEncodedRecordEntry>(*this);
		ar& m_Value;
	}

public:
	BoolEncodedRecordEntry() {}
	BoolEncodedRecordEntry(int key, bool val) :
		BaseEncodedRecordEntry(key),
		m_Value(val)
	{}
	~BoolEncodedRecordEntry() {}

	bool getValue() { return m_Value; }

private:
	bool m_Value;
};

class StrEncodedRecordEntry : public BaseEncodedRecordEntry
{
	friend class boost::serialization::access;
	virtual std::string description() const override
	{
		return m_Value;
	}
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		// save/load base class information
		ar& boost::serialization::base_object<BaseEncodedRecordEntry>(*this);
		ar& m_Value;
	}

public:
	StrEncodedRecordEntry() {}
	StrEncodedRecordEntry(int key, std::string val) :
		BaseEncodedRecordEntry(key),
		m_Value(val)
	{}
	~StrEncodedRecordEntry() {}

	std::string getValue() { return m_Value; }

private:
	std::string m_Value;
};

class EncodedRecord
{
	friend class boost::serialization::access;
	friend std::ostream& operator<<(std::ostream& os, const EncodedRecord& er);
	std::list<BaseEncodedRecordEntry*> m_EncodedRecordEntries;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		ar.register_type(static_cast<IntEncodedRecordEntry*>(NULL));
		ar.register_type(static_cast<BoolEncodedRecordEntry*>(NULL));
		ar.register_type(static_cast<StrEncodedRecordEntry*>(NULL));

		ar& m_EncodedRecordEntries;
	}
public:
	EncodedRecord() {}
	std::list<BaseEncodedRecordEntry*>& getEncodedRecordEntries() { return m_EncodedRecordEntries; }
	void append(BaseEncodedRecordEntry* pBere)
	{
		m_EncodedRecordEntries.insert(m_EncodedRecordEntries.end(), pBere);
	}
	void clear()
	{
		for (auto encodedRecordEntry : m_EncodedRecordEntries)
			delete encodedRecordEntry;
	}
};

class EncodedRecordMapEntry
{
	friend std::ostream& operator<<(std::ostream& os, const EncodedRecordMapEntry& erme);
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int /* file_version */) {
		ar& m_EncodedKey;
		ar& m_OriginalKey;
	}

public:
	EncodedRecordMapEntry() {}
	EncodedRecordMapEntry(int encodedKey, std::string originalKey) : m_EncodedKey(encodedKey), m_OriginalKey(originalKey) {}
	~EncodedRecordMapEntry() {}

	int getEncodedKey() { return m_EncodedKey; }
	std::string getOriginalKey() { return m_OriginalKey; }

protected:
	int m_EncodedKey;
	std::string m_OriginalKey;
};

class EncodedRecordMap
{
	friend class boost::serialization::access;
	friend std::ostream& operator<<(std::ostream& os, const EncodedRecordMap& er);
	std::list<EncodedRecordMapEntry*> m_EncodedRecordMap;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		ar& m_EncodedRecordMap;
	}
public:
	EncodedRecordMap() {}
	std::list<EncodedRecordMapEntry*>& getEncodedRecordMapEntries() { return m_EncodedRecordMap; }
	void append(EncodedRecordMapEntry* pErme)
	{
		m_EncodedRecordMap.insert(m_EncodedRecordMap.end(), pErme);
	}
	void clear()
	{
		for (auto encodedRecordMapEntry : m_EncodedRecordMap)
			delete encodedRecordMapEntry;
	}
};

/*************************/
/*** Record Info class ***/
/*************************/
class RecordInfo
{
	friend std::ostream& operator<<(std::ostream& os, const RecordInfo& er);
public:
	Record& getRecord() { return m_Record; }
	EncodedRecord& getEncodedRecord() { return m_EncodedRecord; }
	EncodedRecordMap& getEncodedRecordMap() { return m_EncodedRecordMap; }
	void clear()
	{
		m_Record.clear();
		m_EncodedRecord.clear();
		m_EncodedRecordMap.clear();
	}

private:
	Record m_Record;
	EncodedRecord m_EncodedRecord;
	EncodedRecordMap m_EncodedRecordMap;
};


