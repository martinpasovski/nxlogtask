// NXLogAssesmentTask.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "JsonParser.h"
#include "RecordService.h"
#include <iostream>
#include <fstream>
#include <sstream>

std::string toLower(const std::string& str)
{
    std::string copy(str);
    transform(copy.begin(), copy.end(), copy.begin(), ::tolower);
    return copy;
}

int main()
{
    std::cout << "Application ENTRY!";

    std::string inputFileName = "input.json";
    std::ifstream ifs(inputFileName);
    std::string inputLine;
    while (std::getline(ifs, inputLine))
    {
        JsonParser jsonParser;
        RecordService recordService;
        std::stringstream jsonRecord;
        jsonRecord << toLower(inputLine.c_str()) << "\n";

        std::cout << "\n\nProcess JSON record... Parse... Encode...\n";

        // Parse the json input for a record
        auto recordInfo = jsonParser.processJsonRecord(inputFileName, jsonRecord);

        // Encode the record (replace and map the keys)
        recordService.encodeRecord(recordInfo);

        std::cout << recordInfo;

        std::cout << "\n\nSave record to binary archive...\n";

        // Save record to binary archive
        std::string encodedRecordsFileName = "encoded_records.txt";
        std::string encodedKeysFileName = "encoded_keys.txt";
        recordService.saveBinaryRecord(recordInfo, encodedRecordsFileName.c_str(), true);
        recordService.saveBinaryRecord(recordInfo, encodedKeysFileName.c_str(), false);

        std::cout << "\nLoad new record from binary archive...\n";

        // Load new record from binary archive
        RecordInfo newRecordInfo;
        recordService.loadBinaryRecord(newRecordInfo, encodedRecordsFileName.c_str(), true);
        recordService.loadBinaryRecord(newRecordInfo, encodedKeysFileName.c_str(), false);

        // Decode the record from the loaded binary record encoded record and coresponding map
        recordService.decodeRecord(newRecordInfo);

        std::cout << newRecordInfo;

        recordInfo.clear();
        newRecordInfo.clear();
    }

    ifs.close();

    std::cout << "\n\nApplication EXIT!\n";

}


