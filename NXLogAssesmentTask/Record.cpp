#include "Record.h"

BOOST_SERIALIZATION_ASSUME_ABSTRACT(BaseRecordEntry)
std::ostream& operator<<(std::ostream& os, const BaseRecordEntry& bre)
{
	return os << "\tKey: " << bre.m_Key << "\tValue: " << bre.description();
}

std::ostream& operator<<(std::ostream& os, const Record& rec)
{
	std::list<BaseRecordEntry*>::const_iterator it;
	for (it = rec.m_RecordEntries.begin(); it != rec.m_RecordEntries.end(); it++) {
		os << "\n\t" << std::hex << "0x" << *it << std::dec << **it;
	}
	return os;
}

BOOST_SERIALIZATION_ASSUME_ABSTRACT(BaseEncodedRecordEntry)
std::ostream& operator<<(std::ostream& os, const BaseEncodedRecordEntry& bere)
{
	return os << "\tEncoded Key: " << bere.m_Key << "\tValue: " << bere.description();
}

std::ostream& operator<<(std::ostream& os, const EncodedRecord& er)
{
	std::list<BaseEncodedRecordEntry*>::const_iterator it;
	for (it = er.m_EncodedRecordEntries.begin(); it != er.m_EncodedRecordEntries.end(); it++) {
		os << "\n\t" << std::hex << "0x" << *it << std::dec << **it;
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, const EncodedRecordMapEntry& sere)
{
	return os << "\tEncoded Key: " << sere.m_EncodedKey << "\tOriginal Key: " << sere.m_OriginalKey;
}

std::ostream& operator<<(std::ostream& os, const EncodedRecordMap& erm)
{
	std::list<EncodedRecordMapEntry*>::const_iterator it;
	for (it = erm.m_EncodedRecordMap.begin(); it != erm.m_EncodedRecordMap.end(); it++) {
		os << "\n\t" << std::hex << "0x" << *it << std::dec << **it;
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, const RecordInfo& er)
{
	os << "\nRecordInfo: " <<
		"\n\tRecord: " << er.m_Record <<
		"\n\tEncoded Record: " << er.m_EncodedRecord <<
		"\n\tEncoded Record Map: " << er.m_EncodedRecordMap;
	return os;
}

