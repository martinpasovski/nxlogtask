#include "RecordService.h"

void RecordService::encodeRecord(RecordInfo & recordInfo)
{
    int index = 0;
    for (auto record : recordInfo.getRecord().getRecordEntries())
    {
        recordInfo.getEncodedRecordMap().append(new EncodedRecordMapEntry(index + 1, (* record).getKey()));

        if (StrRecordEntry* psre = dynamic_cast<StrRecordEntry*>(record)) {
            recordInfo.getEncodedRecord().append(new StrEncodedRecordEntry(index + 1, psre->getValue()));
        }
        else if (BoolRecordEntry* pbre = dynamic_cast<BoolRecordEntry*>(record)) {
            recordInfo.getEncodedRecord().append(new BoolEncodedRecordEntry(index + 1, pbre->getValue()));
        }
        else if (IntRecordEntry* pire = dynamic_cast<IntRecordEntry*>(record)) {
            recordInfo.getEncodedRecord().append(new IntEncodedRecordEntry(index + 1, pire->getValue()));
        }
        index++;
    }
}

void RecordService::decodeRecord(RecordInfo& recordInfo)
{
    std::list<BaseEncodedRecordEntry*>::const_iterator itBere;
    std::list<EncodedRecordMapEntry*>::const_iterator itErme;

    for (int i = 0; i < recordInfo.getEncodedRecordMap().getEncodedRecordMapEntries().size(); i++)
    {
        itBere = std::next(recordInfo.getEncodedRecord().getEncodedRecordEntries().begin(), i);
        itErme = std::next(recordInfo.getEncodedRecordMap().getEncodedRecordMapEntries().begin(), i);

        if (StrEncodedRecordEntry* psere = dynamic_cast<StrEncodedRecordEntry*>(*itBere)) {
            auto strRecordEntry = new StrRecordEntry((*itErme)->getOriginalKey(), psere->getValue());
            recordInfo.getRecord().append(strRecordEntry);
        }
        else if (BoolEncodedRecordEntry* pbere = dynamic_cast<BoolEncodedRecordEntry*>(*itBere)) {
            auto boolRecordEntry = new BoolRecordEntry((*itErme)->getOriginalKey(), pbere->getValue());
            recordInfo.getRecord().append(boolRecordEntry);
        }
        else if (IntEncodedRecordEntry* piere = dynamic_cast<IntEncodedRecordEntry*>(*itBere)) {
            auto intRecordEntry = new IntRecordEntry((*itErme)->getOriginalKey(), piere->getValue());
            recordInfo.getRecord().append(intRecordEntry);
        }
    }
}

void RecordService::saveBinaryRecord(RecordInfo& recordInfo, const char* filename, bool createEncodedRecords)
{
    // make an Encoded Records archive
    std::ofstream ofs(filename);
    boost::archive::text_oarchive oa(ofs);

    if (createEncodedRecords)
        oa << recordInfo.getEncodedRecord();
    else
        oa << recordInfo.getEncodedRecordMap();
}

void RecordService::loadBinaryRecord(RecordInfo& recordInfo, const char* filename, bool restoreEncodedRecords)
{
    // open the Encoded Records archive
    std::ifstream ifs(filename);
    boost::archive::text_iarchive ia(ifs);

    // restore the schedule from the archive
    if (restoreEncodedRecords)
        ia >> recordInfo.getEncodedRecord();
    else
        ia >> recordInfo.getEncodedRecordMap();
}


