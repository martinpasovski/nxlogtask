#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <list>
#include <any>

class C
{
public:
    std::map<std::string, std::any> mc;
};

int main()
{
    std::cout << "Hello World!\n";

    std::any a(12);

    // set any value:
    a = std::string("Hello!");
    a = 16;
    // reading a value:

    // we can read it as int
    std::cout << std::any_cast<int>(a) << '\n';

    // but not as string:
    try
    {
        std::cout << std::any_cast<std::string>(a) << '\n';
    }
    catch (const std::bad_any_cast& e)
    {
        std::cout << e.what() << '\n';
    }

    // reset and check if it contains any value:
    a.reset();
    if (!a.has_value())
    {
        std::cout << "a is empty!" << "\n";
    }

    // you can use it in a container:
    std::map<std::string, std::any> m;
    m["integer"] = 10;
    m["string"] = std::string("Hello World");
    m["float"] = 1.0f;

    for (auto& [key, val] : m)
    {
        if (val.type() == typeid(int))
            std::cout << "int: " << std::any_cast<int>(val) << "\n";
        else if (val.type() == typeid(std::string))
            std::cout << "string: " << std::any_cast<std::string>(val) << "\n";
        else if (val.type() == typeid(float))
            std::cout << "float: " << std::any_cast<float>(val) << "\n";
    }

    std::cout << "\nNow the std::vector<std::any> \n";

    std::vector<std::any> v;
    v.push_back(10);
    v.push_back("Hello World");
    v.push_back(1.0f);

    for (auto& val : v)
    {
        if (val.type() == typeid(int))
            std::cout << "int: " << std::any_cast<int>(val) << "\n";
        else if (val.type() == typeid(std::string))
            std::cout << "string: " << std::any_cast<std::string>(val) << "\n";
        else if (val.type() == typeid(float))
            std::cout << "float: " << std::any_cast<float>(val) << "\n";
    }

    std::cout << "\nNow the std::list<std::map<std::string, std::any>> \n";

    std::list<std::map<std::string, std::any>> l;
    std::map<std::string, std::any> m1, m2;

    m1["integer"] = 10;
    m1["string"] = std::string("Hello World");
    m1["float"] = 1.0f;
    m2["integer"] = 20;
    m2["string"] = std::string("Hi there");
    m2["float"] = 2.0f;
    l.push_back(m1);
    l.push_back(m2);

    for (auto listEntry : l)
    {
        for (auto& [key, val] : listEntry)
        {
            if (val.type() == typeid(int))
                std::cout << "int: " << std::any_cast<int>(val) << "\n";
            else if (val.type() == typeid(std::string))
                std::cout << "string: " << std::any_cast<std::string>(val) << "\n";
            else if (val.type() == typeid(float))
                std::cout << "float: " << std::any_cast<float>(val) << "\n";
        }
    }

    std::cout << "\nNow the std::list<C> where class C holds member std::map<std::string, std::any> \n";
    std::list<C> lc;
    C c1, c2;
    std::map<std::string, std::any> m3, m4;
    m3["integer"] = 10;
    m3["string"] = std::string("Hello World");
    m3["float"] = 1.0f;
    m4["integer"] = 20;
    m4["string"] = std::string("Hi there");
    m4["float"] = 2.0f;

    c1.mc = m1;
    c2.mc = m2;
    lc.push_back(c1);
    lc.push_back(c2);

    for (auto c : lc)
    {
        for (auto& [key, val] : c.mc)
        {
            if (val.type() == typeid(int))
                std::cout << "int: " << std::any_cast<int>(val) << "\n";
            else if (val.type() == typeid(std::string))
                std::cout << "string: " << std::any_cast<std::string>(val) << "\n";
            else if (val.type() == typeid(float))
                std::cout << "float: " << std::any_cast<float>(val) << "\n";
        }
    }

}
